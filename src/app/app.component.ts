import { Component } from '@angular/core';

@Component({
  selector: 'bcp-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'todo-app-bcp';
}
